'use strict';

var mediator = (function () {
	var channels = {};

	function _subscribe (channel, fn) {
		if(!channels.channel) {
			channels.channel = [];
		}

		channels.channel.push({context: this, callback: fn});
	};

	function _publish (channel, color) {
		if(!channels.channel) {
			return false;
		}

		channels.channel.forEach(function (subscriber) {
			subscriber.callback.call(subscriber.context, color);
		});
	};

	return {
		subscribe: _subscribe,
		publish: _publish
	};
})();