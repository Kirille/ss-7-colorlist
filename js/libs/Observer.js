'use strict';

function Observer () {
	var subscriber = '';

	function sub (fn) {
		subscriber = fn;
	}

	function pub () {
		subscriber();
	}

	this.sub = sub;
	this.pub = pub;
	
	return this;
}