'use strict';

document.addEventListener('DOMContentLoaded', init, false);

var tpl = {};

function init () {	
	new ListController();
	new AreaController();
	new LogController();
}