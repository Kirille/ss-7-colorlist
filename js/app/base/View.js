'use strict';

function View () {
	function render (el, tpl, keys) {
		el.innerHTML = template(tpl, keys);

		return el.innerHTML;
	}

	this.render = render;

	return this;
}