'use strict';

function Model (_param) {
	var parameters = _param;	

	this.get = function (value) {
		return parameters[value];
	};

	this.set = function (key, value) {
		parameters[key] = value;
		this.pub();
	};

	this.__proto__ = new Observer();

	return this;
}

