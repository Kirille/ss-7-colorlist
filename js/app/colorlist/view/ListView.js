function ListView (model) {
	this.__proto__ = new View();

	var	color = model.get('color'), 
		el;

	this.init = function () {
		el = document.createElement('div');

		this.render(el, tpl['list'], {class: 'button ' + color});

		el.addEventListener('click', onClick, false);

	}.bind(this);

	this.init();

	function onClick (e) {
		mediator.publish('clicked', color);
	}

	this.getView = function () {
		return el;
	};

	return this;
}