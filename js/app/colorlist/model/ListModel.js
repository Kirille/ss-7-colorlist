'use strict';

function ListModel (_color) {
	var parameters = {
		color: _color
	};

	this.__proto__ = new Model(parameters);

	return this;
}