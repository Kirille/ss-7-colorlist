function ListController () {
	var buttonRed = new ListModel('red'),
		buttonGreen = new ListModel('green'),
		buttonBlue = new ListModel('blue'),
		RedView = new ListView(buttonRed),
		GreenView = new ListView(buttonGreen),
		BlueView = new ListView(buttonBlue)
		el = document.querySelector('#colorlist');

	init();

	function init () {
		el.appendChild(RedView.getView());	
		el.appendChild(GreenView.getView());	
		el.appendChild(BlueView.getView());	
	}	

	return this;
}