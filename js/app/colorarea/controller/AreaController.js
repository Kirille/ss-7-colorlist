function AreaController () {
	var model = new AreaModel(),
		view = new AreaView(model);

	render('white');

	mediator.subscribe('clicked', render);
	
	function render (_color) {
		model.set('color', _color);
	}

	return this;
}