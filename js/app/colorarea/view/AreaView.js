function AreaView (model) {
	this.__proto__ = new View();

	this.init = function () {		
		var	el = document.querySelector('#colorarea'),
			color = model.get('color'),
			template = tpl['area'],
			keys = {class: 'box ' + color, text: color};
		
		this.render(el, template, keys);
	}.bind(this);

	this.init();

	model.sub(this.init);

	return this;
}