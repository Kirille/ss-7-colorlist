'use strict';

function LogController () {
	var	redModel = new LogModel('red'),
		greenModel = new LogModel('green'),
		blueModel = new LogModel('blue'),
		redView = new LogView(redModel),
		greenView = new LogView(greenModel),
		blueView = new LogView(blueModel);

	mediator.subscribe('clicked', counter);
	
	function counter (color) {
		switch(color) {
			case 'red': 
				increment(redModel);
				break;
			case 'green': 
				increment(greenModel);
				break;	
			case 'blue': 
				increment(blueModel);
				break;
		}

		function increment (model) {
			model.set('count', model.get('count') + 1);
		}
	}

	return this;
}	
