'use strict';

function LogView (model) {
	this.__proto__ = new View();

	this.init = function () {		
		var	el = document.querySelector('#log'),
			color = model.get('color'),
			template = tpl['area'],
			keys = {class: 'text ' + color, text: model.get('count')},
			elCheck = el.querySelector('.' + color);

		if (elCheck) { 
			elCheck.innerHTML = this.render(elCheck, template, keys);
		} else { 
			el.innerHTML += this.render(el, template, keys);
		}
	}.bind(this);

	this.init();

	model.sub(this.init);

	return this;
}